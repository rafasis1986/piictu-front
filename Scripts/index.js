(function(window, angular, undefined) {'use strict';
    var loginModule = angular.module('login', ['angularSpinner']);
    var urlAuth = 'http://127.0.0.1:8000/api-token-auth/';
    var urlAfiliate = 'http://localhost:8000/user/create/?format=json';

    loginModule.config(function($controllerProvider, $provide, $httpProvider) {
        loginModule.controller = $controllerProvider.register;
        loginModule.factory = $provide.factory;
        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    });

    loginModule.controller('loginController', function ($scope, $templateCache, $http, $location, $window, usSpinnerService, $rootScope) {
        init();

        function init() {
            $scope.template = '';
            $scope.html = '';
            $scope.page = { url: 'reddits.html', href: 'reddits.html' };
            $scope.user = {username: '', password: '', email: ''};
            $scope.iHaveUser = true;
            $scope.error = '';
            $scope.error_afiliate = '';
            $scope.spinneractive = false;
            
        };

        
        $scope.login = function () {
            $scope.startSpin();
            $http({ method: 'POST',
                url: urlAuth,
                data: $scope.user }).success(function(data){
                    $window.sessionStorage.token = data.token;
                    $scope.stopSpin();
                    $scope.loadPage();
                }).error(function(data){
                    $scope.stopSpin();
                    delete $window.sessionStorage.token;
                    $scope.error = 'Error: Invalid user or password';
                });                
        };

        $scope.loadPage = function () {
            if ($scope.page.href) {
                document.location = $scope.page.href;
            }
            $scope.template = $scope.page.url;
            $http({ method: 'GET', url: $scope.page.url, cache: $templateCache })
              .success(function (html) {
                  $scope.html = html;
                  $('textarea').text(html);
              })
              .error(function (html, status) {
                  $scope.html = 'Unable to load code: ' + status;
              });
        };

        $scope.changeHaveUser = function(){
            if($scope.iHaveUser){
                $scope.iHaveUser = false;    
            }else{
                $scope.iHaveUser = true;
            }
            $scope.user = {username: '', password: '', password_verify: '', email: ''};
        };

        $scope.affiliate = function(){
            $scope.startSpin();
            $scope.error_afiliate = '';
            if($scope.user.password != $scope.user.password_verify){
                $scope.error_afiliate = 'You must input the same password in both fields';
                $scope.user.password = '';
                $scope.user.password_verify = '';
                $scope.stopSpin();
                return false;
            }
            $http({ method: 'POST',
                url: urlAfiliate,
                data: $scope.user}).success(function(data){
                    $scope.stopSpin();
                    $scope.login();
                }).error(function(data){
                    $scope.stopSpin();
                    delete $window.sessionStorage.token;
                    $scope.error_afiliate = data.message;
                });
        };

        $scope.startSpin = function() {
          if (!$scope.spinneractive) {
            usSpinnerService.spin('spinner-1');
          }
        };

        $scope.stopSpin = function() {
          if ($scope.spinneractive) {
            usSpinnerService.stop('spinner-1');
          }
        };

        $rootScope.$on('us-spinner:spin', function(event, key) {
          $scope.spinneractive = true;
        });

        $rootScope.$on('us-spinner:stop', function(event, key) {
          $scope.spinneractive = false;
        });

    });
    
})(window, window.angular);