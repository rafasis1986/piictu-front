﻿app.controller('NavbarController', function ($scope, $location) {
    $scope.getClass = function (path) {
        if ($location.path().substr(0, path.length) == path) {
            return true
        } else {
            return false;
        }
    }
});


app.controller('MyRedditsController',
    function ($scope, $http, $window, uiGridConstants, usSpinnerService) {

    var urlAllReddits = 'http://localhost:8000/subs/lists/';
    var urlAffiliateReddit = 'http://127.0.0.1:8000/reddits/affiliate/';
    var urlDisAffiliateReddit = 'http://127.0.0.1:8000/reddits/disaffiliate/';
    init();

    function init() {
        $scope.errors = [];
        $scope.redditsData = [];
        $scope.name_add = '';
        $scope.name_remove = '';
        $http.defaults.headers.post.Authorization = 'JWT '+$window.sessionStorage.token;
        getAllReddits();
        $scope.gridOptions = {
            data: 'redditsData',
            enablePinning: true,
            enableFiltering: true,
            columnDefs: [
                {field: 'title', width: '30%'},
                {field: 'subscribers', width: '20%'},
                {field: 'url'}
            ]
        };
    };

    function getAllReddits() {
        usSpinnerService.spin('spinner-1');
        $http({ method: 'POST',
                url: urlAllReddits,
                data: $scope.reddit }).success(function(data){
                    usSpinnerService.stop('spinner-1');
                    $scope.redditsData = data;
                }).error(function(data){
                    usSpinnerService.stop('spinner-1');
                    console.log('Error: '+data)
        });
    };

    $scope.canAffiliate = function(){
        if ($scope.redditsData.length < 10){
            return true;
        }
        return false;
    };

    $scope.canDisaffiliate = function(){
        if ($scope.redditsData.length  > 0){
            return true;
        }
        return false;
    };

    $scope.addSubReddit = function () {
        usSpinnerService.spin('spinner-1');
        $scope.errors = [];
        $scope.error_affiliate = '';
        var subsList = [$scope.name_add];
        $http({ method: 'POST',
                url: urlAffiliateReddit,
                data: {'subreddits_list':subsList} }).success(function(data){
                    angular.forEach(data.subreddits_list, function(item){
                       if(! item.success){
                            $scope.errors.push(item.error);
                       }
                    });
                    angular.forEach($scope.errors, function(item){
                        if ($scope.error_affiliate){
                            $scope.error_affiliate = $scope.error_affiliate + " "+ item;
                        }
                        else{
                            $scope.error_affiliate = item;    
                        }
                    });
                    usSpinnerService.stop('spinner-1');
                    init();
            }).error(function(data){
                usSpinnerService.stop('spinner-1');
                $scope.error_affiliate = data.detail;
        });
    };

    $scope.removeSubReddit = function() {
        usSpinnerService.spin('spinner-1');
        $scope.errors = [];
        $scope.error_disaffiliate = '';
        var subsList = [$scope.name_remove];
        $http({ method: 'POST',
                url: urlDisAffiliateReddit,
                data: {'subreddits_list':subsList} }).success(function(data){
                    angular.forEach(data.subreddits_list, function(item){
                       if(! item.success){
                            $scope.errors.push(item.error);
                       }
                    });
                    angular.forEach($scope.errors, function(item){
                        if ($scope.error_disaffiliate){
                            $scope.error_disaffiliate = $scope.error_disaffiliate + " "+ item;
                        }
                        else{
                            $scope.error_disaffiliate = item;    
                        }
                    });
                    usSpinnerService.stop('spinner-1');
                    init();
            }).error(function(data){
                usSpinnerService.stop('spinner-1');
                $scope.error_disaffiliate = data.detail;
        });
    };
    



});

app.controller('CheckRedditsController',function ($scope, $window, $http, usSpinnerService) {
    var urlCheckReddits = 'http://127.0.0.1:8000/reddits/';
    init();

    function init(){
        $http.defaults.headers.post.Authorization = 'JWT '+$window.sessionStorage.token;
        $scope.listAlternatives = [];
        $scope.reddit = {name : ''};
        $scope.error = '';
    };
    
    $scope.geListReddits = function() {
        usSpinnerService.spin('spinner-1');
        $http({ method: 'POST',
            url: urlCheckReddits,
            data: $scope.reddit }).success(function(data){
                $scope.listAlternatives = JSON.parse(data);
                if ($scope.listAlternatives.length == 0){
                    $scope.error = "No occurrences were found for the requested reddit";
                }
                else{
                    $scope.error = '';
                }
                usSpinnerService.stop('spinner-1');
            }).error(function(data){
                $scope.error = data.detail;
                usSpinnerService.stop('spinner-1');
            });

    };
});

app.controller('SubmRedditController',function ($scope, $window, $http, uiGridConstants, usSpinnerService) {

    var urlSubmissionsReddit = 'http://127.0.0.1:8000/reddits/news/';
    init();

    
    function init() {
        $scope.submissionsData = [];
        $scope.reddit = {name : ''};
        $scope.error = '';
        $http.defaults.headers.post.Authorization = 'JWT '+$window.sessionStorage.token;
        $scope.gridSubmissions = {
            data: 'submissionsData',
            enableFiltering: true,
            paginationPageSizes: [10, 25, 50],
            paginationPageSize: 10,
            columnDefs: [
                {field: 'title', width: '35%'},
                {field: 'score', width: '10%'},
                {field: 'link'}
            ]
        };
    };

    
    $scope.getSubmissions = function() {
        usSpinnerService.spin('spinner-1');
        $scope.submissionsData = [];
        $scope.error = '';
        $http({ method: 'POST',
            url: urlSubmissionsReddit,
            data: {'name': $scope.reddit.name, 'limit_posts': 100} }).success(function(data){
                usSpinnerService.stop('spinner-1');
                $scope.submissionsData = JSON.parse(data.posts_list);
                if ($scope.submissionsData.length == 0){
                    $scope.error = "No submissions were found for the requested reddit";    
                }
            }).error(function(data){
                usSpinnerService.stop('spinner-1');
                $scope.error = data.message;
            });
    };
});


app.controller('LogOutController', function ($scope, $templateCache, $http, $location, $window) {
    init();

    function init() {
        $scope.page = { url: 'index.html', href: 'index.html' };
    };
        

    $scope.logout = function(){
        delete $window.sessionStorage.token;
        document.location = $scope.page.href;
        $scope.template = $scope.page.url;
        $http({ method: 'GET', url: $scope.page.url, cache: $templateCache })
          .success(function (html) {
              $scope.html = html;
              $('textarea').text(html);
          })
          .error(function (html, status) {
              $scope.html = 'Unable to load code: ' + status;
          });
    };
});
