﻿'use strict';
var app = angular.module('redditApp', ['ngRoute', 'ngTouch', 'ui.grid', 'ui.grid.pagination', 'angularSpinner']);

//This configures the routes and associates each route with a view and a controller
app.config(function ($routeProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    $routeProvider
        .when('/myReddits',
            {
                controller: 'MyRedditsController',
                templateUrl: 'app/partials/myReddits.html'
            })
        .when('/checkReddit',
            {
                controller: 'CheckRedditsController',
                templateUrl: 'app/partials/checkReddit.html'
            })
        .when('/submReddit',
            {
                controller: 'SubmRedditController',
                templateUrl: 'app/partials/submReddit.html'
            })
        .when('/logOut',
            {
                controller: 'LogOutController',
                templateUrl: 'app/partials/logout.html'
            })
        .otherwise({ redirectTo: '/' });

        
});




